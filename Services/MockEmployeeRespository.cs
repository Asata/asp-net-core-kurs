﻿using SuperKurs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperKurs.Services
{
    public class MockEmployeeRespository : IEmployeeRespository
    {

        private List<Employee> list;
        
        public MockEmployeeRespository()
        {
            list = new List<Employee>() { new Employee { Id = 1, Name = "John", Email = "john@mail.com", Department = Dept.HR }, 
                   new Employee { Id = 2, Name = "Irene", Email = "Irene@mail.com", Department = Dept.IT }, 
                   new Employee { Id = 3, Name = "Jack", Email = "Jack@mail.com", Department = Dept.Payroll } };
        }

        public Employee Add(Employee employee)
        {

            employee.Id = list.Max(employee => employee.Id) + 1;
            list.Add(employee);
            return employee;
        }

        public Employee GetEmployee(int id)
        {
            return list.FirstOrDefault(e => e.Id == id);
        }

        public Employee Update(Employee employeeChanges)
        {

            Employee employee = list.FirstOrDefault(e => e.Id == employeeChanges.Id);
            if (employee != null)
            {
                employee.Name = employeeChanges.Name;
                employee.Email = employeeChanges.Email;
                employee.Department = employeeChanges.Department;
            }

            return employee;
        }

        public Employee Delete(int id)
        {
            Employee employee = list.FirstOrDefault(e => e.Id == id);
            if(employee != null)
            {
                list.Remove(employee);
            }

            return employee;
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            return list;
        }
    }
}
