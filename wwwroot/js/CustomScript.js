﻿
//POKAZUJE YES/NO GDY CHCEMY USUNAC UZYTKOWNIKA/ROLE POPRZEZ LISTUSERS/LISTROLES W ADMINISTRACJI - ZOSTALO ZAIMPLEMENTOWANE NA KONCU TEGO VIEW

function confirmDelete(uniqueId, isDeleteClicked) {

    var deleteSpan = 'deleteSpan_' + uniqueId;

    var confirmDeleteSpan = 'confirmDeleteSpan_' + uniqueId;


    if (isDeleteClicked) {

        $('#' + deleteSpan).hide();
        $('#' + confirmDeleteSpan).show();

    } else {

        $('#' + deleteSpan).show();
        $('#' + confirmDeleteSpan).hide();

    }

}