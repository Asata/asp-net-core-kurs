﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperKurs.Models;
using SuperKurs.Security;
using SuperKurs.Services;
using SuperKurs.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SuperKurs.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private readonly IDataProtector protector;  //do DataProtectionPurposeStrings
        private readonly IEmployeeRespository _employeeRespository;
        private readonly IWebHostEnvironment hostingEnvironment;    

        public HomeController(IEmployeeRespository employeeRespository, IWebHostEnvironment hostingEnvironment, IDataProtectionProvider dataProtectionProvider, DataProtectionPurposeStrings dataProtectionPurposeStrings)
        {


            this.hostingEnvironment = hostingEnvironment;
            _employeeRespository = employeeRespository;
            protector = dataProtectionProvider.CreateProtector(dataProtectionPurposeStrings.EmployeeIdRootValue);   //do DataProtectionPurposeStrings

        }

        [AllowAnonymous]
        public ViewResult Index()
        {

            var model = _employeeRespository.GetAllEmployees()     //do DataProtectionPurposeStrings (wszystko po getallemployees()
                                            .Select(e =>
                                            {
                                                e.EncryptedId = protector.Protect(e.Id.ToString());
                                                return e;
                                            });

            return View(model);
        }

        [AllowAnonymous]
        public ViewResult Details(string id)          //zmiana z int? id na string ze wzgledu na kodowanie id DataProtectionPurposeStrings
        {

            string decryptedId = protector.Unprotect(id);           //dla DataProtectionPurposeStrings
            int employeeId = Convert.ToInt32(decryptedId);

            Employee employee = _employeeRespository.GetEmployee(employeeId);     //dla DataProtectionPurposeStrings zmiana z id.Value na employeeId

            if (employee == null)
            {

                Response.StatusCode = 404;

                return View("EmployeeNotFound", employeeId);

            }


            

            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel();
            homeDetailsViewModel.Employee = employee;
            homeDetailsViewModel.PageTitle = "Employee Details";



            //ViewData["Employee"] = model;
            ViewData["PageTitle"] = "Employee Details";

            return View(homeDetailsViewModel);

        }


        [AllowAnonymous]
        public ViewResult Details(int id)          
        {


            Employee employee = _employeeRespository.GetEmployee(id);  

            if (employee == null)
            {

                Response.StatusCode = 404;

                return View("EmployeeNotFound", id);

            }

            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel();
            homeDetailsViewModel.Employee = employee;
            homeDetailsViewModel.PageTitle = "Employee Details";



            //ViewData["Employee"] = model;
            ViewData["PageTitle"] = "Employee Details";

            return View(homeDetailsViewModel);

        }


        [HttpGet]
        public ViewResult Create()
        {

            return View();

        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateViewModel model)
        {
            if (ModelState.IsValid)
            {

                string uniqueFileName = ProcessUploadedFile(model);
                
                Employee newEmployee = new Employee
                {
                    Name = model.Name,
                    Email = model.Email,
                    Department = model.Department,
                    PhotoPath = uniqueFileName
                };
                _employeeRespository.Add(newEmployee);

                return RedirectToAction("details", new { id = newEmployee.Id });
            }

            return View();

        }


        [HttpGet]
        public ViewResult Edit(int id)
        {
            Employee employee = _employeeRespository.GetEmployee(id);
            EmployeeEditViewModel employeeEditViewModel = new EmployeeEditViewModel
            {

                Id = employee.Id,
                Name = employee.Name,
                Email = employee.Email,
                Department = employee.Department,
                ExistingPhotoPath = employee.PhotoPath,

            };

            return View(employeeEditViewModel);

        }

        [HttpPost]
        public IActionResult Edit(EmployeeEditViewModel model)
        {

            if (ModelState.IsValid)
            {

                Employee employee = _employeeRespository.GetEmployee(model.Id);
                employee.Name = model.Name;
                employee.Email = model.Email;
                employee.Department = model.Department;
                
                if(model.Photo != null)
                {
                    if(model.ExistingPhotoPath!=null)
                    {
                        string filePath = Path.Combine(hostingEnvironment.WebRootPath, "images", model.ExistingPhotoPath);
                        System.IO.File.Delete(filePath);
                    }

                    employee.PhotoPath = ProcessUploadedFile(model);
                }

                _employeeRespository.Update(employee);

                return RedirectToAction("details", employee);
            }

            return View();
        }


        public IActionResult Delete(int id)

        {

            Employee employee = _employeeRespository.GetEmployee(id);

            EmployeeEditViewModel employeeDeleteModel = new EmployeeEditViewModel
            {

                Id = employee.Id,
                Name = employee.Name,
                Email = employee.Email,
                Department = employee.Department,
                ExistingPhotoPath = employee.PhotoPath,

            };



                if (employeeDeleteModel.ExistingPhotoPath != null)
                {
                    string filePath = Path.Combine(hostingEnvironment.WebRootPath, "images", employeeDeleteModel.ExistingPhotoPath);
                    System.IO.File.Delete(filePath);
                }

            
            _employeeRespository.Delete(id);
            return RedirectToAction("Index", _employeeRespository.GetAllEmployees());
        }


        private string ProcessUploadedFile(EmployeeCreateViewModel model)
        {
            string uniqueFileName = null;
            if (model.Photo != null)
            {


                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using(var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.Photo.CopyTo(fileStream);
                }
                

            }

            return uniqueFileName;
        }

        private string ProcessUploadedFile(EmployeeEditViewModel model)
        {
            string uniqueFileName = null;
            if (model.Photo != null)
            {


                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                model.Photo.CopyTo(new FileStream(filePath, FileMode.Create));

            }

            return uniqueFileName;
        }
    }
}
