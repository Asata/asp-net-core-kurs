﻿using SuperKurs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperKurs.Models
{
    public class SQLEmployeeRepository : IEmployeeRespository
    {

        private readonly AppDbContext context1;

        public SQLEmployeeRepository(AppDbContext context)
        {

            this.context1 = context;

        }

        public Employee Add(Employee employee)
        {
            context1.Employees.Add(employee);
            context1.SaveChanges();
            return employee;
        }

        public Employee Delete(int id)
        {
            Employee employee = context1.Employees.Find(id);
            if(employee != null)
            {
                context1.Employees.Remove(employee);
                context1.SaveChanges();
            }

            return employee;
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            return context1.Employees;
        }

        public Employee GetEmployee(int id)
        {
            Employee employee = context1.Employees.Find(id);
            return employee;
        }

        public Employee Update(Employee employeeChanges)
        {

            var employee = context1.Employees.Attach(employeeChanges);
            employee.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context1.SaveChanges();
            return employeeChanges;

        }
    }
}
