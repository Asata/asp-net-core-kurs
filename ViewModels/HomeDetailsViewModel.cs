﻿using SuperKurs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperKurs.ViewModels
{
    public class HomeDetailsViewModel
    {
        //DTO

        public Employee Employee { get; set;}

        public string PageTitle { get; set; }

    }
}
